/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

module.exports = {
    networks: {
        development: {
            host: "localhost",	//truffle development
            port: 9545,
            network_id: "*",
			gas: 4500000,
			gasPrice: 10000000000
         },
		 private: {
            host: "192.168.100.227",	//个人私有节点
            port: 8545,
            network_id: "*",
			gas: 4500000,
			gasPrice: 10000000000
         },
		 test: {
            host: "192.168.12.231",	//公司测试环境私有节点
            port: 8545,
            network_id: "*"
         }
    }
};

